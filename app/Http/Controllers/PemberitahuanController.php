<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Question;

class PemberitahuanController extends Controller
{
    public function index()
    {
        $userId = Auth::id();
        
        $questions = Question::where('id_userqa', $userId)
                             ->where('is_answered', true)
                             ->with(['user', 'admin'])
                             ->get();

        return view('notifikasi', ['questions' => $questions]);
    }

    public function showAnswer($questionId)
    {
        $question = Question::with(['user', 'admin'])->findOrFail($questionId);

        if ($question->id_userqa !== Auth::id()) {
            abort(403, 'Unauthorized action.');
        }

        return view('jawaban', ['question' => $question]);
    }
}
