<style>
    .btn-outline {
        border: 1px solid #ced4da;
        padding: 0.375rem 0.75rem;
        border-radius: 0.25rem;
    }
</style>

<section id="detailkegiatan">
    <div class="card container mb-5">
        <div class="card-header bg-white mt-3">
            <h5 class="card-title">Detail Kegiatan</h5>
        </div>
        <div class="card-body">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td class="col-2">
                            <h6>Nama Ketua</h6>
                        </td>
                        <td class="col-1 text-center">:</td>
                        <td class="col-9">{{ $kegiatan->users()->where('jabatan', 'Ketua')->first()->nama }}</td>
                    </tr>
                    <tr>
                        <td class="col-2">
                            <h6>Nama Anggota</h6>
                        </td>
                        <td class="col-1 text-center">:</td>
                        <td class="col-9">
                            <ul class="list-unstyled">
                                @foreach ($kegiatan->users()->where('jabatan', 'Anggota')->get() as $anggota)
                                    <li>{{ $anggota->nama }}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-2">
                            <h6>Nama Dosen Pembimbing</h6>
                        </td>
                        <td class="col-1 text-center">:</td>
                        <td class="col-9">{{ $kegiatan->users()->where('jabatan', 'Dosen')->first()->nama }}</td>
                    </tr>
                    <tr>
                        <td class="col-2">
                            <h6>Nama Sekolah</h6>
                        </td>
                        <td class="col-1 text-center">:</td>
                        <td class="col-9">{{ $kegiatan->sekolah }}</td>
                    </tr>
                    <tr>
                        <td class="col-2">
                            <h6>Tanggal Pelaksanaan</h6>
                        </td>
                        <td class="col-1 text-center">:</td>
                        <td class="col-9">{{ $kegiatan->tanggal_kegiatan }}</td>
                    </tr>
                    <tr>
                        <td class="col-2">
                            <h6>Status Laporan</h6>
                        </td>
                        <td class="col-1 text-center">:</td>
                        <td class="col-9">
                            @foreach ($laporans as $laporan)
                                @if ($laporan->status_promosi == 'Diterima')
                                    <span class="badge bg-success px-3 p-2">{{ $laporan->status_promosi }}</span>
                                @elseif($laporan->status_promosi == 'Diproses')
                                    <span class="badge bg-info px-3 p-2">{{ $laporan->status_promosi }}</span>
                                @elseif($laporan->status_promosi == 'Ditolak')
                                    <span class="badge bg-danger px-3 p-2" data-bs-toggle="modal"
                                        data-bs-target="#laporanditolak{{ $laporan->id }}">
                                        {{ $laporan->status_promosi }}
                                    </span>

                                    <!-- Modal for Ditolak -->
                                    <div class="modal fade" id="laporanditolak{{ $laporan->id }}"
                                        data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                                        aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-body bg-light p-5">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-2 d-flex align-items-center">
                                                                <img style="width: 75px; height: 75px;"
                                                                    src="{{ asset('img/denied.png') }}" alt="logo">
                                                            </div>
                                                            <div class="col-8">
                                                                <div class="row">
                                                                    <div class="col-12">
                                                                        <h3 class="fw-bold">Laporan Anda Ditolak
                                                                        </h3>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <h6 class="text-secondary">Laporan anda
                                                                            ditolak dikarenakan:</h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-2 d-flex justify-content-end">
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 pt-5">
                                                                <p class="form-control border border-secondary h-100 bg-light"
                                                                    rows="4" readonly>
                                                                    {{ $laporan->catatan }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer bg-danger border-0"></div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </td>

                    </tr>
                </tbody>
            </table>
        </div>

        <div class="card-header bg-white mt-3">
            <h5 class="card-title">File Pendukung</h5>
            <div class="card-body">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            @forelse ($filePendukung as $file)
                        <tr>
                            <td class="col-2">
                                <h6>{{ $file->nama }}</h6>
                            </td>
                            <td class="col-1 text-center">:</td>
                            <td class="col-9">
                                <a href="{{ $file->url }}" target="_blank" id="downloadLink"
                                    class="badge bg-dark text-decoration-none">
                                    <i data-feather="download"></i> Download
                                </a>
                            </td>
                        </tr>
                    @empty
                        <!-- Tidak ada file -->
                        @endforelse

                        {{-- JIKA LAPORAN BERSTATUS SELESAI BISA UNDUH SERTIFIKAT  --}}
                        @if ($laporanDiterima)
                            <tr>
                                <td class="col-2">
                                    <h6>Sertifikat</h6>
                                </td>
                                <td class="col-1 text-center">:</td>
                                <td class="col-9">
                                    <a href="{{ route('buat', ['id' => $kegiatan->id]) }}"
                                        class="badge bg-dark text-decoration-none">
                                        <i data-feather="download"></i> Download
                                    </a>
                                </td>
                            </tr>
                        @else
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif


                    </tbody>
                </table>
            </div>
        </div>

        <div class="card-header bg-white mt-3">
            <h5 class="card-title">Laporan Kegiatan</h5>
        </div>
        <div class="card-body">
            @if ($laporanBelumUpload)
                <button type="button" class="badge bg-success text-decoration-none border-0" data-bs-toggle="modal"
                    data-bs-target="#tambahlaporan">
                    <i data-feather="plus"></i> Tambah Laporan
                </button>
            @elseif ($laporan->status_promosi == 'Diproses')
                <span class="badge bg-warning px-3 p-2">Menunggu Konfirmasi</span>
            @else
                {{-- @foreach ($laporans as $laporan)
                    @if ($laporan->status_promosi == 'Ditolak')
                        <button type="button" class="badge bg-warning text-decoration-none border-0 btn-edit-laporan"
                            data-laporanid="{{ $laporan->id }}" data-bs-toggle="modal" data-bs-target="#editlaporan">
                            <i data-feather="edit"></i> Edit Laporan
                        </button>
                    @endif
                @endforeach --}}
            @endif
        </div>

        {{-- TABEL LAPORAN KEGIATAN  --}}
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr class="text-center">
                        <th scope="col">No</th>
                        <th scope="col">Nama File</th>
                        <th scope="col">Tgl Unggah</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($files as $file)
                        <tr class="text-center">
                            <td class="pt-3 pb-3 px-3">{{ $loop->iteration }}</td>
                            <td class="pt-3 pb-3">{{ $file->nama_file }}</td>
                            <td class="pt-3 pb-3">
                                {{ \Carbon\Carbon::parse($file->created_at)->translatedFormat('d F Y') }}</td>
                            <td class="pt-3 pb-3">
                                <a href="{{ url('storage/' . $file->dokumen) }}" target="_blank"
                                    class="badge bg-info text-decoration-none text-white">
                                    <i data-feather="download"></i> Unduh
                                </a>
                                @foreach ($laporans as $laporan)
                                    @if ($laporan->status_promosi == 'Ditolak')
                                        <button type="button"
                                            class="badge bg-warning text-decoration-none text-white btn-edit-laporan"
                                            data-laporanid="{{ $laporan->id }}" data-bs-toggle="modal"
                                            data-bs-target="#editlaporanModal">
                                            <i data-feather="edit"></i> Edit
                                        </button>
                                    @endif
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
    </div>

</section>

<!-- Modal for adding laporan -->
<div class="modal fade" id="tambahlaporan" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title mx-auto" id="uploadModalLabel">Laporan</h3>
            </div>
            <div class="modal-body m-3">
                <form id="form-tambah-laporan" method="POST" enctype="multipart/form-data"
                    action="{{ route('proses_laporan') }}">
                    @csrf
                    <input type="hidden" name="id_kegiatan" value="{{ $kegiatan->id }}">
                    <div class="form-group">
                        <label for="absenFile">
                            <h5>Absen</h5>
                        </label><br>
                        <input type="file" class="form-control-file" id="absenFile" name="file_laporans[]"
                            style="display: none;" accept=".pdf,.docx">
                        <label class="btn btn-light px-6 btn-outline" for="absenFile"><i data-feather="upload"
                                style="color: gray;"></i> upload absen</label>
                    </div>
                    <div class="form-group mt-4">
                        <label for="dokumentasiFile">
                            <h5>Dokumentasi Kegiatan</h5>
                        </label><br>
                        <input type="file" class="form-control-file" id="dokumentasiFile" name="file_laporans[]"
                            style="display: none;" accept=".pdf,.docx">
                        <label class="btn btn-light px-6 btn-outline" for="dokumentasiFile"><i data-feather="upload"
                                style="color: gray;"></i> upload dokumentasi</label>
                    </div>
                </form>
                <button type="button" id="btn-kirim-laporan" class="btn btn-dark mt-5 d-block mx-auto pt-3 pb-3"
                    style="width:85%;">Kirim</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal for editing laporan -->
{{-- <div class="modal fade" id="editlaporan" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title mx-auto" id="uploadModalLabel">Laporan</h3>
            </div>
            <div class="modal-body m-3">
                <form id="form-edit-laporan" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="absenFile">
                            <h5>Absen</h5>
                        </label><br>
                        <input type="file" class="form-control-file" id="absenFile" name="file_laporans[]"
                            style="display: none;" accept=".pdf,.docx" required>
                        <label class="btn btn-light px-6 btn-outline" for="absenFile"><i data-feather="upload"
                                style="color: gray;"></i> upload absen</label>
                    </div>
                    <div class="form-group mt-4">
                        <label for="dokumentasiFile">
                            <h5>Dokumentasi Kegiatan</h5>
                        </label><br>
                        <input type="file" class="form-control-file" id="dokumentasiFile" name="file_laporans[]"
                            style="display: none;" accept=".pdf,.docx" required>
                        <label class="btn btn-light px-6 btn-outline" for="dokumentasiFile"><i data-feather="upload"
                                style="color: gray;"></i> upload dokumentasi</label>
                    </div>
                </form>
                <button type="button" id="btn-edit-laporan" class="btn btn-dark mt-5 d-block mx-auto pt-3 pb-3"
                    style="width:85%;">Kirim</button>
            </div>
        </div>
    </div>
</div> --}}
<!-- Modal for editing laporan -->
<div class="modal fade" id="editlaporanModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title mx-auto" id="uploadModalLabel">Edit Laporan</h3>
            </div>
            <div class="modal-body m-3">
                <form id="form-edit-laporan" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="editAbsenFile">
                            <h5>Absen</h5>
                        </label><br>
                        <input type="file" class="form-control-file" id="editAbsenFile" name="file_laporans[]"
                            style="display: none;" accept=".pdf,.docx">
                        <label class="btn btn-light px-6 btn-outline" for="editAbsenFile">
                            <i data-feather="upload" style="color: gray;"></i> upload absen
                        </label>
                    </div>
                    <div class="form-group mt-4">
                        <label for="editDokumentasiFile">
                            <h5>Dokumentasi Kegiatan</h5>
                        </label><br>
                        <input type="file" class="form-control-file" id="editDokumentasiFile"
                            name="file_laporans[]" style="display: none;" accept=".pdf,.docx">
                        <label class="btn btn-light px-6 btn-outline" for="editDokumentasiFile">
                            <i data-feather="upload" style="color: gray;"></i> upload dokumentasi
                        </label>
                    </div>
                </form>
                <button type="button" id="btn-edit-laporan" class="btn btn-dark mt-5 d-block mx-auto pt-3 pb-3"
                    style="width:85%;">Kirim</button>
            </div>
        </div>
    </div>
</div>

<!-- AJAX script -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        $('#btn-kirim-laporan').on('click', function() {
            var formData = new FormData($("#form-tambah-laporan")[0]);

            $.ajax({
                type: 'POST',
                url: '{{ route('proses_laporan') }}',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response); // Log respons ke console
                    alert(response.message);
                    window.location.reload();
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    console.error('Error - ' + errorMessage);
                    console.error(xhr.responseText); // Log respons kesalahan
                    alert('Error - ' + errorMessage);
                }
            });
        });

        //     $('.btn-edit-laporan').on('click', function() {
        //         var laporanId = $(this).data('laporanid');
        //         $('#form-edit-laporan').attr('action', '{{ route('edit_laporan', '') }}/' + laporanId);
        //     });

        //     $('#btn-edit-laporan').on('click', function() {
        //         var formData = new FormData($("#form-edit-laporan")[0]);

        //         $.ajax({
        //             type: 'POST',
        //             url: $("#form-edit-laporan").attr('action'),
        //             data: formData,
        //             cache: false,
        //             contentType: false,
        //             processData: false,
        //             success: function(response) {
        //                 console.log(response); // Log respons ke console
        //                 alert(response.message);
        //                 window.location.reload();
        //             },
        //             error: function(xhr, status, error) {
        //                 var errorMessage = xhr.status + ': ' + xhr.statusText;
        //                 console.error('Error - ' + errorMessage);
        //                 console.error(xhr.responseText); // Log respons kesalahan
        //                 alert('Error - ' + errorMessage);
        //             }
        //         });
        //     });

        // js ajax yang bisa
        $(document).ready(function() {
            $('.btn-edit-laporan').on('click', function() {
                var laporanId = $(this).data('laporanid');
                $('#form-edit-laporan').attr('action', '{{ route('edit_laporan', '') }}/' +
                    laporanId);
            });

            $('#btn-edit-laporan').on('click', function() {
                var formData = new FormData($("#form-edit-laporan")[0]);

                $.ajax({
                    type: 'POST',
                    url: $("#form-edit-laporan").attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        console.log(response);
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        console.error('Error - ' + errorMessage);
                        console.error(xhr.responseText);
                        alert('Error - ' + errorMessage);
                    }
                });
            });
        });
    });
</script>
