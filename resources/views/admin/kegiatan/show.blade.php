@extends('admin.dashboard.layouts.main')

@php
    $title = 'Kegiatan';
@endphp

@section('title')
    Dashboard Lihat Kegiatan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    {{-- <div class="container" style="margin-top: 100px;">
        <h3>Detail Kegiatan</h3>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Nama Ketua: {{ $ketuaName }}</h5>
                <h6 class="card-subtitle mb-2 text-muted">Nama Anggota:</h6>
                <ul>
                    @foreach ($anggotaNames as $name)
                        <li>{{ $name }}</li>
                    @endforeach
                </ul>
                <h6 class="card-subtitle mb-2 text-muted">Nama Dosen Pembimbing: {{ $dosenName }}</h6>
                <p class="card-text">Nama Sekolah: {{ $sekolah }}</p>
                <p class="card-text">Tanggal Kegiatan: {{ $tanggalKegiatan }}</p>
            </div>
        </div>
    </div> --}}

    <div class="col-11 col-sm-10 col-md-9 mx-auto mb-5 p-4 p-sm-5 border"
        style="background-color: rgb(255, 255, 255); margin-top: 125px; border-radius: 10px">
        <h1 class="fs-5 text-center mb-5 pb-2 border-bottom border-2">Detail Kegiatan</h1>

        <div>
            <h5 class="card-title mb-3">Nama Ketua : {{ $ketuaName }}</h5>
            <h6 class="card-subtitle text-secondary mb-2">Nama Anggota :</h6>
            <ul>
                @foreach ($anggotaNames as $name)
                    <li>{{ $name }}</li>
                @endforeach
            </ul>
            <h6 class="card-subtitle text-secondary mb-3">Nama Dosen Pembimbing : {{ $dosenName }}</h6>
            <p class="card-text">Nama Sekolah : {{ $sekolah }}</p>
            <p class="card-text">Tanggal Kegiatan : {{ $tanggalKegiatan }}</p>
        </div>
    </div>
@endsection