<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\UserManagement;

class UserManagementController extends Controller
{
    public function index()
    {
        $users = UserManagement::select('id', 'nama', 'nim', 'email', 'role', 'prodi')->get();
        return view('admin.dashboard.user.index', compact('users'));
    }
    public function search(Request $request)
    {
        $search = $request->input('search');

        $users = UserManagement::query()
            ->where('nama', 'like', '%' . $search . '%')
            ->orWhere('nim', 'like', '%' . $search . '%')
            ->orWhere('email', 'like', '%' . $search . '%')
            ->orWhere('prodi', 'like', '%' . $search . '%')
            ->orWhere('role', 'like', '%' . $search . '%')
            ->paginate(5);

        return view('admin.dashboard.user.index', compact('users', 'search'));
    }

    public function create()
    {
        return view('admin.dashboard.user.create');
    }

    public function store(Request $request)
    {
        $request->merge(['password' => Hash::make($request->password)]);
        UserManagement::create($request->all());

        return redirect()->route('usersmanagement.index')->with('success', 'User berhasil ditambahkan!');
    }

    public function edit($id)
    {
        $user = UserManagement::findOrFail($id);
        return view('admin.dashboard.user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = UserManagement::findOrFail($id);
        $user->update($request->only(['nama', 'nim', 'email', 'role', 'prodi']));
        return redirect()->route('usersmanagement.index')->with('success', 'Informasi pengguna berhasil diperbarui!');
    }

    public function destroy(UserManagement $user)
    {
        $user->delete();

        return redirect()->route('usersmanagement.index')->with('success', 'User berhasil dihapus!');
    }
}
