@extends('admin.dashboard.layouts.main')

@php
    $title = 'Laporan';
@endphp

@section('title')
    Dashboard Laporan
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="col-11 mx-auto mb-5 border overflow-hidden"
        style="background-color: rgb(255, 255, 255); font-size: 13px; margin-top: 125px; border-radius: 10px">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center p-4">
            <h1 class="fs-5 mb-3 mb-sm-0">Data Laporan</h1>
            {{-- <a href="{{ url('/dashboard-tambah-laporan') }}" class="btn btn-success align-self-end"
                style="border-radius: 25px">
                <span>+</span>
                <span>Tambah Data</span>
            </a> --}}
        </div>
        {{-- <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center text-secondary px-4">
            <form action="{{ route('admin.dashboard.laporan.search') }}" method="post" class="">
                @csrf
                <label for="search">Search :</label>
                <input type="text" name="search" id="search" class="border border-2 px-1"
                    style="width: 200px; border-radius: 5px;">
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div> --}}
        @if (session('success'))
            <div class="mt-3 alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        {{-- Table --}}
        <div class="table-responsive px-3 pb-3">
            <table class="table table-hover mt-3" id="table-laporan">
                <thead class="table-light border-top border-bottom">
                    <tr>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">NO</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">NAMA KETUA</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">NAMA SEKOLAH</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">FILE LAPORAN</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">TANGGAL LAPORAN</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">STATUS</th>
                        <th class="text-secondary fw-semibold px-3 text-nowrap">CATATAN</th>
                        <th class="text-secondary fw-semibold text-center px-3 text-nowrap">AKSI</th>
                    </tr>
                </thead>

                <tbody>
                    @forelse ($laporans as $laporan)
                        <tr>
                            <td class="text-secondary text-center px-3 text-nowrap">{{ $loop->iteration }}</td>
                            <td class="px-3 text-nowrap">
                                @foreach ($laporan->kegiatan->users->where('pivot.jabatan', 'Ketua') as $user)
                                    {{ $user->nama }}
                                @endforeach
                            </td>
                            <td class="px-3 text-nowrap">{{ $laporan->kegiatan->sekolah }}</td>
                            <td class="px-3 text-nowrap">
                                @foreach ($laporan->files as $file)
                                    <a href="{{ asset('storage/' . $file->dokumen) }}">{{ $file->nama_file }}</a><br>
                                @endforeach
                            </td>
                            <td class="px-3 text-nowrap">{{ $laporan->tanggal_laporan }}</td>
                            <td class="px-3 text-center text-nowrap">
                                @if ($laporan->status_promosi == 'Proses')
                                    <span class="badge text-bg-secondary fw-normal pb-2"
                                        style="font-size: 13px">Proses</span>
                                @elseif($laporan->status_promosi == 'Diterima')
                                    <span class="badge text-bg-success fw-normal pb-2"
                                        style="font-size: 13px">Diterima</span>
                                @elseif($laporan->status_promosi == 'Ditolak')
                                    <span class="badge text-bg-danger fw-normal pb-2" style="font-size: 13px">Ditolak</span>
                                @else
                                    <span class="badge text-bg-secondary fw-normal pb-2"
                                        style="font-size: 13px">{{ $laporan->status_promosi }}</span>
                                @endif
                            </td>
                            <td class="px-3 text-nowrap">{{ $laporan->catatan }}</td>
                            <td class="text-center px-3 text-nowrap">
                                <!-- Form with button to trigger modal -->
                                {{-- <form action="#" method="get" class="d-inline">
                                    <button type="button" name="lihat" class="btn text-secondary fs-5 mx-1"
                                        data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                                        <i class="bi bi-eye"></i>
                                    </button>
                                </form> --}}

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                ...
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <form action="{{ route('admin.laporan.edit', $laporan->id) }}" method="get"
                                    class="d-inline">
                                    <button type="submit" name="edit" class="btn  text-secondary fs-5 mx-1">
                                        <i class="bi bi-pencil-square"></i>
                                    </button>
                                </form>
                                <form action="{{ route('admin.laporan.delete', $laporan->id) }}" method="post"
                                    class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" name="hapus" class="btn text-secondary fs-5 mx-1"
                                        onclick="return confirm('Apakah Anda yakin ingin menghapus kegiatan ini?')">
                                        <i class="bi bi-trash3"></i>
                                    </button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- Table End --}}

        {{-- <div class="d-flex flex-column flex-md-row justify-content-between align-items-center text-secondary p-4">
            <div>
                Showing {{ $laporans->firstItem() }} to {{ $laporans->lastItem() }} of {{ $laporans->total() }} entries
            </div>
            <nav class="mt-5 mt-md-0">
                <ul class="pagination mb-0">
                    @if ($laporans->onFirstPage())
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link text-secondary" href="{{ $laporans->previousPageUrl() }}">
                                <i class="bi bi-chevron-left" style="margin-right: 5px;"></i>Prev
                            </a>
                        </li>
                    @endif

                    @for ($i = 1; $i <= $laporans->lastPage(); $i++)
                        <li class="page-item {{ $i == $laporans->currentPage() ? 'active' : '' }}">
                            <a class="page-link" href="{{ $laporans->url($i) }}">{{ $i }}</a>
                        </li>
                    @endfor

                    @if ($laporans->hasMorePages())
                        <li class="page-item">
                            <a class="page-link text-secondary" href="{{ $laporans->nextPageUrl() }}">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @else
                        <li class="page-item disabled">
                            <a class="page-link text-secondary">
                                Next<i class="bi bi-chevron-right" style="margin-left: 5px;"></i>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div> --}}
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/2.0.8/css/dataTables.dataTables.min.css">
@endpush

@push('js')
    <script src="//cdn.datatables.net/2.0.8/js/dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table-laporan').DataTable();
        });
    </script>
@endpush