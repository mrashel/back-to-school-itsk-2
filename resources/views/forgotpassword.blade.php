@extends('login.loginform')

@section('title', 'BTS-ITSK | Forgot Password')

@section('container')

    <div class="col-12 col-sm-10 col-xl-8 p-4 p-sm-5">
        <h1 class="fs-2">Kesulitan Masuk?</h1>
        <p class="fw-semibold mb-5">
            Masukkan email anda dan kami akan mengirimkan tautan untuk kembali ke akun anda!
        </p>

        @if (Session::has('message'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('message') }}
            </div>
        @endif

        <form action="{{ route('forget.password.post') }}" method="post">
            @csrf
            <label for="email" class="form-label fw-semibold mb-0">Email</label>
            <input type="email" class="form-control mb-5 p-2" id="email" placeholder="Masukkan email" name="email"
                required>

            @if ($errors->has('email'))
                <span class="text-danger">{{ $errors->first('email') }}</span>
            @endif

            <button type="submit" class="btn btn-dark w-100 p-2">Kirim</button>
        </form>
    </div>

@endsection
